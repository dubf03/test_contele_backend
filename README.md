## Requirements
- Git
- Docker

## Clonar o projeto
- git clone https://dubf03@bitbucket.org/dubf03/contele_backend_test.git
- cd contele_backend_test

## Configurar Laradock
- git submodule init
- git submodule update
- cd path_to_project/laradock
- cp env-example .env

## Instalar o mongodb nos containers Workspace e PHP-FPM: 
- Abrir o arquivo .env 
- Procurar por WORKSPACE_INSTALL_MONGO e alterar o valor para true
- Procurar por PHP_FPM_INSTALL_MONGO e alterar o valor para true

## Re-build dos containers
- docker-compose build workspace php-fpm

## Iniciar os dockers
- docker-compose up -d redis mongo nginx

## Acessar workspace e instalar as dependencias
- docker-compose exec workspace bash
- composer require jenssegers/mongodb
- composer install

## Rodando as migrations
- php artisan migrate
- exit

## Importando arquivo com as coordenadas 
- docker cp /home/eduardo/Projetos/Contele/contele_backend_test/locations.json laradock_mongo_1:/tmp/locations.json
- docker exec laradock_mongo_1 mongoimport -d database -c locations --file /tmp/locations.json --jsonArray

## URL de exemplo: http://local.contele.com/api/locations?zoom=0&min=0&max=5&radius=1000000&view=10&lng=-46.62869783157425&lat=-23.220308056164285
